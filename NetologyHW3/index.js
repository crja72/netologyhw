﻿const ChatApp = require('./chat') // 3.2

let webinarChat = new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat = new ChatApp('---------vk');
vkChat.setMaxListeners(2) // 1.2

let chatOnMessage = (message) => {
    console.log(message);
};

webinarChat.on('message', chatOnMessage);
webinarChat.on('message', prepareAnswer); // 1.1
facebookChat.on('message', chatOnMessage);
vkChat.on('message', chatOnMessage);
vkChat.on('message', prepareAnswer); // 1.3
vkChat.on('close', vkchatOnClose);
vkChat.close(); // 2.3


// Закрыть вконтакте
setTimeout(() => {
    console.log('Закрываю вконтакте...');
    vkChat.removeListener('message', chatOnMessage);
}, 10000);


// Закрыть фейсбук
setTimeout(() => {
    console.log('Закрываю фейсбук, все внимание — вебинару!');
    facebookChat.removeListener('message', chatOnMessage);
}, 15000);

setTimeout(() => { //3.1
    console.log('Отписываемся от сообщений для вебинара');
    webinarChat.removeListener('message', chatOnMessage);
}, 30000);

function prepareAnswer() {
    console.log('Готовлюсь к ответу')
}

function vkchatOnClose() {
    console.log('Чат вконтакте закрылся :('); //2.2
}