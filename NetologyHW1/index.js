'use strict';
const Pokemon = require('./Pokemon');
const PokemonList = require('./PokemonList');

var pikachu = new Pokemon('Pikachu', 10);
var chermonder = new Pokemon('Chermonder', 2)

var lost = new PokemonList(
    new Pokemon('MiTwo', 6),
    new Pokemon('BeeDrill', 1),
    new Pokemon('Slowpok', 99)
);

var found = new PokemonList(pikachu, chermonder);

lost.add('Rapidash', 1);
lost.push(new Pokemon('Grimer', 2));

found.add('Slowbro', 100);
found.push(new Pokemon('Shellder', 4));

console.log("Lost:");
lost.show();
console.log("Found:");
found.show();

found.push(lost.shift());
console.log("Lost after move:");
lost.show();
console.log("Found after move:");
found.show();

console.log("Max level found Pokemon:");
found.max().show();
