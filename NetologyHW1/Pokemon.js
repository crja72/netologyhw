﻿class Pokemon {
    constructor(name, level) {
        this.name = name;
        this.level = level;
    }

    show() {
        console.log(`Pokemon: ${this.name}, level: ${this.level}`);
    }

    valueOf() {
        return this.level;
    }
}

module.exports = Pokemon;