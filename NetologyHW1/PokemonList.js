﻿const Pokemon = require('./Pokemon');

class PokemonList extends Array {
    add(name, level) {
        this.push(new Pokemon(name, level));
    }

    show() {
        console.log(`We have  ${this.length} Pokemons here:`)
        this.forEach(item => item.show());
    }

    max() {
        let max_level = Math.max(...this)
        return this.find((o, index, arr) => o.level === max_level);

    }


}

module.exports = PokemonList;