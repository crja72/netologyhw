﻿"use strict"

const Writable = require('stream').Writable;

class CWritable extends Writable {

    _write(chunk, encoding, done) {
        console.log(`Data - ${chunk}`)
        console.log('-=-=-=-=-=-=-=-=-')
        done();
    }
}

module.exports = { CWritable };