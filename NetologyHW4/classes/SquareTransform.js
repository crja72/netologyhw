﻿"use strict"

const Transform = require('stream').Transform;

/**
 * Do data^2 transform
 */
class SquareTransform extends Transform {

    constructor(options) {
        if (!options) options = new Object();
        options.objectMode = true;
        super(options);
    }

    _transform(chunk, encoding, done) {
        let result = null, error = null;
        try {
            let i = parseInt(chunk.toString())
            result = (i * i).toString();
        } catch (e) {
            error = e.message;
        }
        setTimeout(() => {
            done(error, result);
        }, 1000);
    }

}

module.exports = {
    SquareTransform
};