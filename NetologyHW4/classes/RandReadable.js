﻿"use strict"

const Readable = require("stream").Readable;

class RandReadable extends Readable {
    constructor(options) {
        if (!options) options = new Object();
        options.objectMode = true;
        super(options)
    }

    _read(size) {
        this.push(this.random(1, 100));
    }

    random(max, min) {
        var rand = min + Math.random() * (max + 1 - min);
        return Math.floor(rand)
        
    }
}

module.exports = {
    RandReadable
};