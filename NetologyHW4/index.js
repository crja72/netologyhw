'use strict';

const fs = require('fs');
const crypto = require('crypto');
const stream = require('stream');
const HexTransform = require('./classes/HexTransform').HexTransform;


const RandReadable = require('./classes/RandReadable').RandReadable;
const SquareTransform = require('./classes/SquareTransform').SquareTransform;
const CWritable = require('./classes/CWritable').CWritable;
//first part

const input = fs.createReadStream('.\\data\\input.txt')
const output = fs.createWriteStream('.\\data\\output.txt')
const hash = crypto.createHash('md5');

let hashpipe = input.pipe(hash);
hashpipe.pipe(process.stdout);
hashpipe.pipe(output);

// part two
const input2 = fs.createReadStream('.\\data\\input.txt')
const outputHex = fs.createWriteStream('.\\data\\output_hex.txt')
const hex_tr = new HexTransform()

let hex_pipe = input2.pipe(hex_tr);
hex_pipe.pipe(process.stdout);
hex_pipe.pipe(outputHex);


// additional task

const rr = new RandReadable();
const st = new SquareTransform();
const cw = new CWritable();

console.log();
rr.on('data', (data) => {
    console.log(`Input data - ${data}`);
})

rr.pipe(st).pipe(cw);

