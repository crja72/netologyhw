angular
    .module('PokemonApp')
    .factory('PokemonsService', function($http) {

            $http.defaults.headers.common = {
                'application-id': "93DCDF7F-D029-B9A2-FF88-690D6703D000",
                'secret-key': "6EC9CC06-F2BA-F9CB-FFC0-B4AF3CB73900"
            };

            return {

                getPokemons: function() {
                    return $http.get('http://api.backendless.com/v1/data/pokemon');
                },

                getPokemon: function(pokemonId) {
                    return $http.get('http://api.backendless.com/v1/data/pokemon/' + pokemonId);
                },

                createPokemon: function(pokemonData) {
                    return $http({
                        method: 'POST',
                        url: 'http://api.backendless.com/v1/data/pokemon',
                        data: pokemonData
                    });
                },

                deletePokemon: function(pokemonId) {
                    return $http({
                        method: 'DELETE',
                        url: 'http://api.backendless.com/v1/data/pokemon/' + pokemonId
                    });
                },

                editPokemon: function(id, data) {
                    return $http({
                        method: 'PUT',
                        url: 'http://api.backendless.com/v1/data/pokemon/' + id,
                        data
                    });
                }

            }

        }

    );
