angular
    .module('PokemonApp')
    .factory('PokemonsService', function($http) {

            return {

                getPokemons: function() {
                    return $http.get('http://pokeapi.co/api/v2/pokemon/?limit=10');
                },

                getPokemon: function(pokemonId) {
                    return $http.get('http://pokeapi.co/api/v2/pokemon/' + pokemonId);
                },

                createPokemon: function(pokemonData) {
                    return $http({
                        method: 'POST',
                        url: 'https://api.backendless.com/v1/data/pokemon',
                        headers: {
                            'application-id': "93DCDF7F-D029-B9A2-FF88-690D6703D000",
                            'secret-key': "6EC9CC06-F2BA-F9CB-FFC0-B4AF3CB73900"

                        },
                        data: pokemonData
                    });
                },

                deletePokemon: function(pokemonId) {
                    return $http({
                        method: 'DELETE',
                        url: 'https://api.backendless.com/v1/data/pokemon/' + pokemonId,
                        headers: {
                            'application-id': "93DCDF7F-D029-B9A2-FF88-690D6703D000",
              
                            'secret-key': "6EC9CC06-F2BA-F9CB-FFC0-B4AF3CB73900"

                        }
                    });
                }

            }

        }

    );
