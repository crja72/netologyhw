'use strict';

var MongoClient = require('mongodb').MongoClient;

const URL = 'mongodb://127.0.0.1:27017/names_test';

const show_names = (collection, callback) => {
    collection.find().toArray((err, items) => {
        if (err) return console.error(err);
        console.log(items);
        callback();
    })
}

MongoClient.connect(URL, function (err, db) {
    if (err) return console.error(`Cann't connect to  ${url}. Error - ${err}`);
    console.log('Connected');
    var names = db.collection('names');
    var names_arr = [
        { name: 'John', profession: 'java developer' },
        { name: 'Jack', profession: 'java developer' },
        { name: 'Jimm', profession: 'java developer' },
        { name: 'Jonson', profession: 'java developer' },
        { name: 'James', profession: 'java developer' },
        { name: 'Jacob', profession: 'janitor' },
        { name: 'Jabril', profession: 'janitor' }
    ];
    names.insert(names_arr, function (err, result) {
        if (err) return console.error(err);
        show_names(names, () => {
            names.update({ profession: 'janitor' }, { $set: { profession: 'js developer' } }, { multi: true }, function (err, result) {
                if (err) return console.error(err);
                show_names(names, () => {
                    names.remove({ profession: 'js developer' }, function (err, result) {
                        if (err) return console.error(err);
                        show_names(names, () => {
                            names.remove();
                            db.close();                            
                        })
                    })
                })
            })
        })
    })
})
